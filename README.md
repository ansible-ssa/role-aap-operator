# Ansible Role: Ansible Automation Platform Operator

This role will help to deploy the Ansible Automation Platform in Red Hat OpenShit.

## How to use this role

If you want to use this role in your playbook:

```yaml
---
- name: Install Ansible Automation Platform operator
  hosts: all
  gather_facts: false

  pre_tasks:
    - name: Check OCP input parameters
      ansible.builtin.assert:
        that:
          - "{{ item }} is defined"
        msg: "{{ item }} must be defined"
      with_items:
        - openshit_api
        - openshift_token
        - openshift_namespace
        - openshift_storage_class

  tasks:
    - name: Install and configure Ansible Automation Platform
      block:
        - name: Install and configure Ansible Automation Platform operator
          ansible.builtin.include_role:
            name: role-aap-operator
      environment:
        K8S_AUTH_HOST: "{{ openshit_api }}"
        K8S_AUTH_API_KEY: "{{ openshift_token }}"
        K8S_AUTH_VERIFY_SSL: False
```

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.

## Run the playbook:

```sh
ansible-navigator run <your_playbooks>.yml -m stdout \
  -e 'ansible_python_interpreter=/usr/bin/python3' \
  -e 'openshit_api=<OPENSHIFT_URL>' \
  -e 'openshift_token=<OPENSHIFT_API>' \
  -e 'openshit_storage_class=gp3-csi' \
  -e 'openshift_namespace=<OPENSHIFT_NAMESPACE>'
```
